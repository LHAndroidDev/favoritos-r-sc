package hurtado.luis.ejemplo.favoritos.m_DataObject;

/**
 * Created by Luis on 02/09/2017.
 */
public class Datos {

    String imagenurl;
    int  plus, plus48, refurb, nuevo, airplane, freeshi;

  /*  public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    */

    public String getImagenurl() {
        return imagenurl;
    }

    public void setImagenurl(String imagenurl) {
        this.imagenurl = imagenurl;
    }

    /*
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    */

    public int getPlus() {
        return plus;
    }

    public void setPlus(int plus) {
        this.plus = plus;
    }

    public int getPlus48() {
        return plus48;
    }

    public void setPlus48(int plus48) {
        this.plus48 = plus48;
    }

    public int getRefurb() {
        return refurb;
    }

    public void setRefurb(int refurb) {
        this.refurb = refurb;
    }

    public int getNuevo() {
        return nuevo;
    }

    public void setNuevo(int nuevo) {
        this.nuevo = nuevo;
    }

    public int getAirplane() {
        return airplane;
    }

    public void setAirplane(int airplane) {
        this.airplane = airplane;
    }

    public int getFreeshi() {
        return freeshi;
    }

    public void setFreeshi(int freeshi) {
        this.freeshi = freeshi;
    }
}
