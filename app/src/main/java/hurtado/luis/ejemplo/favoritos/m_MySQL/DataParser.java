package hurtado.luis.ejemplo.favoritos.m_MySQL;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.GridView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import hurtado.luis.ejemplo.favoritos.Favoritos;
import hurtado.luis.ejemplo.favoritos.m_DataObject.Datos;
import hurtado.luis.ejemplo.favoritos.m_UI.CustomAdapter;

/**
 * Created by Luis on 02/09/2017.
 */
public class DataParser extends AsyncTask<Void,Void,Boolean> {

    Context c;
    String jsonData;
    GridView gv;
    ArrayList<Datos> losdatos = new ArrayList<>();
    ProgressDialog pd;

    public DataParser(Context c, String jsonData, GridView gv) {
        this.c = c;
        this.jsonData = jsonData;
        this.gv = gv;
    }

    //public static int Contador;
   int Contador;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        pd = new ProgressDialog(c);
        pd.setTitle("Parseando");
        pd.setMessage("Parseando ... Espere porfavor");
        pd.show();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        return this.parseData();
    }

    @Override
    protected void onPostExecute(Boolean parsed) {
        super.onPostExecute(parsed);

        pd.dismiss();
        Favoritos.indicador.setText("Mis favoritos (" + Contador +")");

        if (parsed){

            //Se llama al adaptador
            CustomAdapter adapter = new CustomAdapter(c,losdatos);
            gv.setAdapter(adapter);

        }else{
            Toast.makeText(c,"No se pudo Parsear",Toast.LENGTH_SHORT).show();
        }
    }

    private Boolean parseData(){
        try
        {
            JSONArray js= new JSONArray(jsonData);
            JSONObject jo;

            losdatos.clear();
            Datos datos;

            Contador = js.length();



            for (int i = 0; i<js.length(); i++){
                jo = js.getJSONObject(i);

                String imagneurl =jo.getString("imagen");
               // String nombre = jo.getString("nombre");
                //int id = jo.getInt("id");
                int plus = jo.getInt("plus");
                int plus48 = jo.getInt("plus48");
                int refurb = jo.getInt("refurb");
                int nuevo = jo.getInt("new");
                int airplane = jo.getInt("airplane");
                int freeshi = jo.getInt("freeshi");

                datos=new Datos();

                datos.setImagenurl(imagneurl);
               // datos.setNombre(nombre);
               // datos.setId(id);
                datos.setPlus(plus);
                datos.setPlus48(plus48);
                datos.setRefurb(refurb);
                datos.setNuevo(nuevo);
                datos.setAirplane(airplane);
                datos.setFreeshi(freeshi);

                losdatos.add(datos);

            }



            return true;


        } catch (JSONException e) {
            e.printStackTrace();
        }
    return false;
    }



}
