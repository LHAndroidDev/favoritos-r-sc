package hurtado.luis.ejemplo.favoritos.m_MySQL;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.GridView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

/**
 * Created by Luis on 02/09/2017.
 */
public class Downloader extends AsyncTask<Void, Void, String> {

    Context c;
    String urlAdress;
    GridView gv;

    ProgressDialog pd;

    public Downloader(Context c, String urlAdress, GridView gv) {
        this.c = c;
        this.urlAdress = urlAdress;
        this.gv = gv;
    }

    @Override
    protected void onPreExecute() {
        pd = new ProgressDialog(c);
        pd.setTitle("Descargando");
        pd.setMessage("Descargando ... Espere porfavor");
        pd.show();
    }

    @Override
    protected String doInBackground(Void... voids) {
        return this.downloadData();
    }

    @Override
    protected void onPostExecute(String jsonData) {
        super.onPostExecute(jsonData);

        pd.dismiss();
if (jsonData==null)
{
    Toast.makeText(c,"No se encontraron datos",Toast.LENGTH_SHORT).show();
}else{


    //ACA VIENE EL PARSEO DE LA DATA
    DataParser parser = new DataParser(c,jsonData,gv);
    parser.execute();
}

    }

    private String downloadData() {

        HttpURLConnection con = Connector.connect(urlAdress);

        if (con == null) {


            return null;
        }

        try{
            InputStream is = new BufferedInputStream(con.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line;
            StringBuffer jsonData = new StringBuffer();

            while ((line=br.readLine()) != null)
            {
                jsonData.append(line+"\n");
            }

            br.close();
            is.close();

            return  jsonData.toString();


        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}


