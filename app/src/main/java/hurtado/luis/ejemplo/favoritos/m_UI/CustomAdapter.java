package hurtado.luis.ejemplo.favoritos.m_UI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import hurtado.luis.ejemplo.favoritos.Favoritos;
import hurtado.luis.ejemplo.favoritos.R;
import java.util.ArrayList;
import hurtado.luis.ejemplo.favoritos.m_UI.*;
import hurtado.luis.ejemplo.favoritos.m_DataObject.Datos;

/**
 * Created by Luis on 02/09/2017.
 */
public class CustomAdapter extends BaseAdapter {

    Context c;
    ArrayList<Datos> losdatos;

    public CustomAdapter(Context c, ArrayList<Datos> losdatos) {
        this.c = c;
        this.losdatos = losdatos;
    }

    @Override
    public int getCount() {
        return losdatos.size();
    }

    @Override
    public Object getItem(int position) {
        return losdatos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            convertView = LayoutInflater.from(c).inflate(R.layout.model,parent,false);
        }

        ImageView productimg = (ImageView)convertView.findViewById(R.id.ImgProducto);
        ImageView imgplus = (ImageView)convertView.findViewById(R.id.plus);
        ImageView imgplus48 = (ImageView)convertView.findViewById(R.id.plus48);
        ImageView imgrefurb = (ImageView)convertView.findViewById(R.id.refurb);
        ImageView imgnuevo = (ImageView)convertView.findViewById(R.id.news);
        ImageView imgairplane = (ImageView)convertView.findViewById(R.id.airplane);
        ImageView imgfreeshi = (ImageView)convertView.findViewById(R.id.freeship);

        Favoritos.indicador2.setText("Tenemos ("+getCount()+") Itemes");

        Datos dt = (Datos)this.getItem(position);

        PicassoClient.downloadImage(c,dt.getImagenurl(),productimg);

        int PLUS = dt.getPlus();
        int PLUS48 = dt.getPlus48();
        int REFURB = dt.getRefurb();
        int NUEVO = dt.getNuevo();
        int AVION = dt.getAirplane();
        int FREESHI = dt.getFreeshi();

        if (PLUS == 1){
        imgplus.setImageResource(R.drawable.nd_ic_plus_30);
        }
        if (PLUS48 == 1){
            imgplus48.setImageResource(R.drawable.nd_ic_plus_48_30);
        }
        if (REFURB == 1){
            imgrefurb.setImageResource(R.drawable.nd_ic_refurbished_30);
        }
        if (NUEVO == 1){
            imgnuevo.setImageResource(R.drawable.nd_ic_new_30);
        }
        if (AVION == 1){
            imgairplane.setImageResource(R.drawable.nd_ic_international_30);
        }
        if (FREESHI == 1){
            imgfreeshi.setImageResource(R.drawable.nd_ic_free_shipping_30);
        }

        return convertView;
    }
}
