package hurtado.luis.ejemplo.favoritos.m_UI;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import hurtado.luis.ejemplo.favoritos.R;

/**
 * Created by Luis on 02/09/2017.
 */
public class PicassoClient {

    public static void downloadImage (Context c,String imageURL,ImageView img){

        if (imageURL != null && imageURL.length()>0){
            Picasso.with(c).load(imageURL).placeholder(R.drawable.nd_ic_add_fav_on_32).into(img);
        }
        else{
            Picasso.with(c).load(R.drawable.nd_ic_add_fav_on_32).into(img);
        }
    }
}
